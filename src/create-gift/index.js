const DYNAMODB = require("aws-sdk/clients/dynamodb");

const dbClient = new DYNAMODB({
  region: "us-east-1",
});

exports.handler = async (event) => {
  const queueItems = event.Records.map((record) => record.body);

  for (const item of queueItems) {
      const message = JSON.parse(item)
      const body = JSON.parse(message.Message) 

    const dbParams = {
      ExpressionAttributeNames: {
        "#gift": "gift",
      },
      ExpressionAttributeValues: {
        ":gift": {
          S: _getGift(body.birthdate),
        },
      },
      Key: {
        "dni": {
          S: body.dni,
        },
      },
      TableName: "clients",
      UpdateExpression: "SET #gift = :gift"
    };

    try {
      const dbResult = await dbClient.updateItem(dbParams).promise();
      console.info(dbResult);
    } catch (error) {
      console.error(error);
      return {
        statusCode: 500,
        body: error,
      };
    }
  }

  return {
    statusCode: 200,
    body: "success",
  };
};

function _getGift(birthdate){
    if (_isSummer(birthdate)) {
        return 'remera';
    }

    if (_isAutumn(birthdate)) {
        return 'buzo';
    }

    if (_isWinter(birthdate)) {
        return 'sweater';
    }

    if (_isSpring(birthdate)) {
        return 'camisa';
    }
}

/**
 * from 12/21 to 03/20
 * @param {string} date 
 * @returns 
 */
function _isSummer(date) {
    const dateParts = date.split('-');
    const formattedDate = dateParts[1] + '-' + dateParts[2];
    return (formattedDate >= '12-21' && formattedDate <= '12-31') || (formattedDate >= '01-01' && formattedDate <= '03-20');
}

/**
 * from 03/21 to 06/20
 * @param {string} date 
 * @returns 
 */
function _isAutumn(date) {
    const dateParts = date.split('-');
    const formattedDate = dateParts[1] + '-' + dateParts[2];

    return formattedDate >= '03-21' && formattedDate <= '06-20';
}

/**
 * from 06/21 to 09/20
 * @param {string} date 
 * @returns 
 */
function _isWinter(date) {
    const dateParts = date.split('-');
    const formattedDate = dateParts[1] + '-' + dateParts[2];
    return formattedDate >= '06-21' && formattedDate <= '09-20';
}

/**
 * from 09/21 to 12/20
 * @param {string} date 
 * @returns 
 */
function _isSpring(date) {
    const dateParts = date.split('-');
    const formattedDate = dateParts[1] + '-' + dateParts[2];
    return formattedDate >= '09-21' && formattedDate <= '12-20';
}
  